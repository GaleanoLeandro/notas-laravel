# Comenzar proyecto:

- Situarse con la terminal en la carpeta deseada

- iniciar proyecto: **_laravel new projectName_**

- Crear base de datos a la cual conectarse

- Ajustar configuración de archivo **.env**
    ~~~
    APP_NAME=CursoLaravel
    APP_ENV=local
    APP_KEY=base64:rjYKQ7JZBN/NSB3ZN0H3ocYW2yBetL6ifwLNSRNoheM=
    APP_DEBUG=true
    APP_URL=http://laravel.dev.com

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=laravel-db
    DB_USERNAME=root
    DB_PASSWORD=
    ~~~
    
- Crear virtual host
    - Añadir nombre de virtual host al archivo **C:\Windows\System32\drivers\etc\hosts**:
        **127.0.0.1 laravel.dev.com**
    - Agregar virtual host en apache httpd.conf
        ~~~
        # Comments
        <VirtualHost *:80>
         ServerName laravel.dev.com
         DocumentRoot "C:\Users\andyn\Desktop\Andy\Proyectos\Curso\public"
         ErrorLog "C:\Users\andyn\Desktop\Andy\Proyectos\Curso\error_log"
         CustomLog "C:\Users\andyn\Desktop\Andy\Proyectos\Curso\access_log" common
        
        </VirtualHost>
        
        <Directory "C:\Users\andyn\Desktop\Andy\Proyectos\Curso\public">
               Options Indexes FollowSymLinks
               AllowOverride All
               Require all granted
        </Directory>
        ~~~

    - Guardar cambios y reiniciar apache

